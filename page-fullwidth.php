<?php
/**
 * The template for displaying Full Width pages.
 *
 * Template Name: Full Width (No Sidebar)
 *
 * @package Bose
 */

get_header(); ?>
<div class="header-title col-md-12">
    <span>
        <?php the_title(); ?>
    </span>
</div>
<div id="primary-mono" class="<?php if (is_account_page() && !is_user_logged_in()): echo 'content-area col-md-8'; else : echo 'content-area col-md-12'; endif ?>">
    <main id="main" class="site-main" role="main">

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('content', 'page'); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            if (comments_open() || '0' != get_comments_number()) :
                comments_template();
            endif;
            ?>

        <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
